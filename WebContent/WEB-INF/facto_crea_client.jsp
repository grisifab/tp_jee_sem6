<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form method="post" action="CreationClient">
		<div>Formulaire d'ajout d'un Client</div>
		<div><label for="nom">Nom *</label>
			<input type="text" id="nom" name="nom" value="${nom}" />
		</div>
		<div><label for="prenom">Pr�nom *</label>
			<input type="text" id="prenom" name="prenom" value="${prenom}" />
		</div>
		<div><label for="telephone">T�l�phone *</label>
			<input type="text" id="telephone" name="telephone" value="${telephone}" />
		</div>
		<div><label for="email">email *</label>
			<input type="text" id="email" name="email" value="${email}" />
		</div>
		<input type="submit"value="Cr�er" />
	</form>
	
	<h3><c:out value="${ erreur }"></c:out></h3>
	
</body>
</html>