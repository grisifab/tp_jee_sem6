package org.eclipse.dao;

import java.sql.*;
import org.eclipse.models.*;

public class ClientDAOImpl implements DAO<Client> {

	@Override
	public void save(Client o) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				c.setAutoCommit(false);
				PreparedStatement ps = c.prepareStatement("insert into client (nom,prenom,telephone,email) values (?,?,?,?); ",
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, o.getNom());
				ps.setString(2, o.getPrenom());
				ps.setString(3, o.getTelephone());
				ps.setString(4, o.getEmail());
				ps.executeUpdate();
				c.commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}	
	}
}
