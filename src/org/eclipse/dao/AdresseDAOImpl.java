package org.eclipse.dao;

import java.sql.*;
import org.eclipse.models.*;

public class AdresseDAOImpl implements DAO<Adresse> {
	
	@Override
	public void save(Adresse o) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				c.setAutoCommit(false);
				PreparedStatement ps = c.prepareStatement("insert into adresse (nomRue,codePostal,ville,client) values (?,?,?,?); ",
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, o.getNomRue());
				ps.setString(2, o.getCodePostal());
				ps.setString(3, o.getVille());
				ps.setString(4, o.getClient().getNom());
				ps.executeUpdate();
				c.commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}	
	}
}
