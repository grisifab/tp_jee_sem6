package org.eclipse.controller;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.models.*;
import org.eclipse.controller.Att;
import org.eclipse.dao.AdresseDAOImpl;
import org.eclipse.dao.ClientDAOImpl;

@WebServlet("/CreationAdresse")
public class CreationAdresse extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	public static ArrayList<Adresse> listeDeAdresse= new ArrayList<Adresse>();     
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {				
		this.getServletContext().getRequestDispatcher("/WEB-INF/creerAdresse.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		boolean test1, test2, test3, test4, test5, test6, test7, test8;
		String erreur = new String();
		ClientDAOImpl monCDI = new ClientDAOImpl();
		AdresseDAOImpl monADI = new AdresseDAOImpl();
		
		String nom = request.getParameter(Att.ATT_NOM);
		test1 = Att.verifChaine(nom);
		String prenom = request.getParameter(Att.ATT_PRENOM);
		test2 = Att.verifChaine(prenom);
		String telephone = request.getParameter(Att.ATT_TEL);
		test3 = Att.verifNumero(telephone);
		String email = request.getParameter(Att.ATT_MEL);
		test4 = Att.verifMail(email);
		String nomRue = request.getParameter(Att.ATT_RUE);
		test5 = Att.verifChaine(nomRue);
		String codePostal = request.getParameter(Att.ATT_CP);
		test6 = Att.verifNumero(codePostal);
		String ville = request.getParameter(Att.ATT_VIL);
		test7 = Att.verifChaine(ville);
		test8 = test1 && test2 && test3 && test4 && test5 && test6 && test7 ;
		
		if(!test1) {
			erreur += "Champ nom incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_NOM, nom);
		}
		
		if(!test2) {
			erreur += "Champ pr�nom incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_PRENOM, prenom);
		}
		
		if(!test3) {
			erreur += "Champ t�l�phone incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_TEL, telephone);
		}
		
		if(!test4) {
			erreur += "Champ email incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_MEL, email);
		}
		
		if(!test5) {
			erreur += "Champ nom de rue incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_RUE, nomRue);
		}
		
		if(!test6) {
			erreur += "Champ code postal incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_CP, codePostal);
		}
		
		if(!test7) {
			erreur += "Champ ville incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_VIL, ville);
		}
		
		
		if(test8) {
			Client client = new Client(nom,prenom,telephone,email);
			monCDI.save(client);
			Adresse adresse = new Adresse(nomRue, codePostal, ville, client);
			monADI.save(adresse);
			listeDeAdresse.add(adresse);
			request.setAttribute(Att.ATT_ADR, adresse);
			erreur = " ";
		}
		
		request.setAttribute(Att.ATT_ERR, erreur);
		
		this.getServletContext().getRequestDispatcher(test6 ? Att.VUE_ADR : Att.VUE_CADR).forward(request, response);
	}

}
