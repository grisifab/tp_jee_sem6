package org.eclipse.controller;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.models.Client;
import org.eclipse.controller.Att;
import org.eclipse.dao.ClientDAOImpl;

/**W
 * Servlet implementation class TestServlet
 */
@WebServlet("/CreationClient")
public class CreationClient extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	public static ArrayList<Client> listeDeClient= new ArrayList<Client>();
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		this.getServletContext().getRequestDispatcher("/WEB-INF/creerClient.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		boolean test1, test2, test3, test4, test5;
		String erreur = new String();
		ClientDAOImpl monCDI = new ClientDAOImpl();
		
		String nom = request.getParameter(Att.ATT_NOM);
		test1 = Att.verifChaine(nom);
		String prenom = request.getParameter(Att.ATT_PRENOM);
		test2 = Att.verifChaine(prenom);
		String telephone = request.getParameter(Att.ATT_TEL);
		test3 = Att.verifNumero(telephone);
		String email = request.getParameter(Att.ATT_MEL);
		test4 = Att.verifMail(email);
		
		test5 = test1 && test2 && test3 && test4;
		
		if(!test1) {
			erreur += "Champ nom incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_NOM, nom);
		}
		
		if(!test2) {
			erreur += "Champ pr�nom incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_PRENOM, prenom);
		}
		
		if(!test3) {
			erreur += "Champ t�l�phone incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_TEL, telephone);
		}
		
		if(!test4) {
			erreur += "Champ email incorrect ; \n";
		} else {
			request.setAttribute(Att.ATT_MEL, email);
		}
		
		if(test5) {
			Client client = new Client(nom,prenom,telephone,email);	
			listeDeClient.add(client);
			request.setAttribute(Att.ATT_CLI, client);
			erreur = " ";
			monCDI.save(client);
		}
		
		request.setAttribute(Att.ATT_ERR, erreur);
		
		this.getServletContext().getRequestDispatcher(test4 ? Att.VUE_CLI : Att.VUE_CCLI).forward(request, response);
	}

}
